from flask import request, jsonify
from flask_restful import Resource
from algorithm.straighten_with_dlc import straighten
from cv2 import cv2
from api import limiter
import numpy as np
import base64


class FindSalamanderInfo(Resource):
    """
    Endpoint for processing the salamanders abdominal pattern.
    POST: Receives an image of a salamander, processes it, and returns the processed image back to the frontend server.
    """

    decorators = [limiter.limit("5/minute")]

    @staticmethod
    def post():
        """
        param image: an base64 image of a salamander
        :return: JSON object containing the processed image or error messages
        """
        json_data = request.json
        image = json_data["image"]
        if image:

            np_array = np.fromstring(base64.b64decode(image), np.uint8)

            img_np = cv2.imdecode(np_array, cv2.IMREAD_COLOR)

            deeplab_image = cv2.cvtColor(img_np, cv2.COLOR_RGB2BGR)

            str_image, _, _, _, _ = straighten(deeplab_image)
            if str_image is not None:
                str_image = cv2.cvtColor(str_image, cv2.COLOR_BGR2RGB)

                _, buffer = cv2.imencode('.jpg', str_image)
                encoded = base64.b64encode(buffer)

                encoded_string = str(encoded)[2:-1]

                return jsonify({"status": 200, "image": encoded_string})
            else:
                return jsonify({"status": 400, "message": "image was not straightened"})
