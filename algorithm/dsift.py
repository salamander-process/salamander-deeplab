import numpy as np
from cv2 import cv2
from algorithm.constants import width, height, step_size

kp = [cv2.KeyPoint(x, y, step_size) for y in range(0, width, step_size) for x in range(0, height, step_size)]
sift = cv2.SIFT_create()


def compute_descriptors(image):
	"""
	Computes the descriptors of an incoming image
	Args:
		image: Image from openCV

	Returns:
		Descriptors of image
	"""

	dense = sift.compute(image, kp)
	des = dense[1]
	return des
