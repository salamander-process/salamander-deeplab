from glob import glob
import cv2


pngs = glob('E:\\PycharmProjects\\SalamanderBilder\\Eirik\\7_orig\\*.JPG')
MAX_DEEPLABCUT_IMAGE_SIZE = 1280

for i, j in enumerate(pngs):
    img = cv2.imread(j)
    # find the largest dimention:
    larger_dim = max(img.shape[1], img.shape[0])
    # reduce size of image by the largest dimention, if it exceeds a given constant:
    if larger_dim > MAX_DEEPLABCUT_IMAGE_SIZE:
        factor = MAX_DEEPLABCUT_IMAGE_SIZE / larger_dim
        img = cv2.resize(img, None, fx=factor, fy=factor, interpolation=cv2.INTER_CUBIC)
    cv2.imwrite('E:\\PycharmProjects\\SalamanderBilder\\Eirik\\7\\img'+str(i)+'.png', img)